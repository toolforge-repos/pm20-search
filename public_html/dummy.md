---
title: PM20 fulltext search
lang: en

---

Search words in PM20 folder titles, aliases and descriptions.

<form submit="pm20-search.php">

<input type="text" id="searchfor" name="searchfor" >


<label for="inc_pe" class="nowrap" >Persons
  <input type="checkbox" id="inc_pe" name="inc_pe" checked>
</label> &#160;
<label for="inc_co" class="nowrap">Companies
  <input type="checkbox" id="inc_co" name="inc_co" checked>
</label> &#160;
<label for="inc_sh" class="nowrap">Countries-Subjects
  <input type="checkbox" id="inc_sh" name="inc_sh" checked>
</label> &#160;
<label for="inc_wa" class="nowrap">Wares
  <input type="checkbox" id="inc_wa" name="inc_wa" checked>
</label>

<input type="checkbox" id="exclude_film_sections" name="exclude_film_sections">
<label for="exclude_film_sections"> Exclude film sections (access limited to EU countries)</label>

<input type="checkbox" id="exclude_meta_only" name="exclude_meta_only" checked>
<label for="exclude_meta_only"> Exclude metadata-only records</label>






</form>
