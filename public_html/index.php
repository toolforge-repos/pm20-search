<?php
/** jneubert, 2023-11-27
 *
 * two modes:
 * 1) empty page => offer search
 * 2) show result list and offer changing search terms
 *
 * TODO not yet implemented:
 *   - search for countries
 *   - search for subjects
 *   - functional restrictions (e.g., 'wa' cannot be combined with other collections)
 */

require __DIR__ . '/vendor/autoload.php';

$debug = 0;

$GLOBALS['collections'] = array( 'pe', 'co', 'sh', 'wa' );
$GLOBALS['collection'] = array(
  'pe' => array(
    'de' => 'Personen',
    'en' => 'Persons',
    ),
  'co' => array(
    'de' => 'Firmen/Organisationen',
    'en' => 'Companies/Organizations',
    ),
  'sh' => array(
    'de' => 'Länder-Sach',
    'en' => 'Countries-Subjects',
    ),
  'wa' => array(
    'de' => 'Waren',
    'en' => 'Commodities/Wares',
  )
);

# allow switch to German by param
if (isset($_REQUEST['lang']) and $_REQUEST['lang'] == "de") {
  $lang = "de";
} else {
  $lang = 'en';
}

# ONE collection may be preselected by GET param
# multiple in POST, otherwise use all collections as default
if (isset($_GET['coll']) and $_GET['coll'] == 'sh_wa') {
  $selected_collections = array('sh', 'wa');
} elseif (isset($_GET['coll']) and in_array($_GET['coll'], $GLOBALS['collections'])) {
  $selected_collections[] = $_GET['coll'];
} elseif (isset($_POST['coll'])) {
  $selected_collections = $_POST['coll'];
} else {
  $selected_collections = $GLOBALS['collections'];
}

$bl_title = ($lang == 'en')
  ? 'PM20 fulltext search'
  : 'PM20 Volltext-Suche';

output_header($lang, $bl_title, $selected_collections);

// global logic
// for GET requests, a page with only header and footer is
// created, no debug info
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  do_search($lang, $selected_collections);
}

output_footer($lang, $debug);

//////////////////

function do_search($lang, $selected_collections) {

  $collections = '"' . join('", "', $selected_collections) . '"';

  // Retrieve form data
  $searchfor = htmlspecialchars($_POST["q"]);
  # require at least 3 characters
  if (mb_strlen($searchfor) < 3) {
    return;
  } elseif (mb_strlen($searchfor) == 3 and mb_substr($searchfor, 2, 1) == '*') {
    return;
  }

  $filter_online = (array_key_exists('exclude_meta', $_POST))
    ? 'filter(bound(?online) && ?online > 0)' : '';

  $query_folder = <<<EOF
select ?item ?itemLabel ?pm20 ?pmLabel ?collection ?online
with {
  select * where {
    bind ("$searchfor" as ?searchfor)
  }
} as %p
with {
  select ?item
  where {
    include %p
    bind (concat("haswbstatement:P4293 ", ?searchfor) as ?searchstr)
    SERVICE wikibase:mwapi {
      bd:serviceParam wikibase:endpoint "www.wikidata.org" .
      bd:serviceParam wikibase:api "Generator" .
      bd:serviceParam mwapi:generator "search" .
      bd:serviceParam mwapi:gsrsearch ?searchstr .
      bd:serviceParam mwapi:gsrlimit "max" .
      bd:serviceParam mwapi:gsrnamespace "0" .
      bd:serviceParam mwapi:gsrprop "" .
      ?item wikibase:apiOutputItem mwapi:title .
    }
  }
} as %i
where {
  include %i
  include %p
  ?item wdt:P4293 ?pm20Id.

  bind(substr(?pm20Id, 1, 2) as ?collection)
  filter(?collection in ($collections))
  bind(uri(concat('https://pm20.zbw.eu/folder/', ?pm20Id)) as ?pm20)

  ?item p:P4293 ?statement .
  ?statement ps:P4293 ?pm20Id .

  # original title (normally in German)
  optional {
    ?statement pq:P1810 ?pmLabel .
  }

  # number of documents online
  optional {
    ?statement pq:P5592 ?online .
  }
  $filter_online

  ?item rdfs:label ?itemLabel .
  filter (lang(?itemLabel)="$lang")

}
EOF;

  $query_ware = <<<EOF
select ?item ?itemLabel ?pm20 ?pmLabel ?collection ?online
with {
  select * where {
    bind ("$searchfor" as ?searchfor)
  }
} as %p
with {
  select ?item
  where {
    include %p
    bind (concat("haswbstatement:P10890 ", ?searchfor) as ?searchstr)
    SERVICE wikibase:mwapi {
      bd:serviceParam wikibase:endpoint "www.wikidata.org" .
      bd:serviceParam wikibase:api "Generator" .
      bd:serviceParam mwapi:generator "search" .
      bd:serviceParam mwapi:gsrsearch ?searchstr .
      bd:serviceParam mwapi:gsrlimit "max" .
      bd:serviceParam mwapi:gsrnamespace "0" .
      bd:serviceParam mwapi:gsrprop "" .
      ?item wikibase:apiOutputItem mwapi:title .
    }
  }
} as %i
where {
  include %i
  include %p
  ?item wdt:P10890 ?wareId.

  bind(uri(concat('https://pm20.zbw.eu/category/ware/i/', ?wareId)) as ?pm20)

##  ?item p:P10890 ?statement .
##  ?statement ps:P10890 ?wareId .

##  # original title (normally in German)
##  optional {
##    ?statement pq:P1810 ?pmLabel .
##  }

  ?item rdfs:label ?itemLabel .
  filter (lang(?itemLabel)="$lang")

  # temporary
  bind(?itemLabel as ?pm20Label)
}
EOF;

  if ( $selected_collections[0] == 'wa' ) {
    $query = $query_ware;
  } else {
    $query = $query_folder;
  }

  ##echo '<pre>', $query, '</pre>';

  $api = new RestClient([
    "headers" => [
      "Content-type" => "application/sparql-query; charset=UTF-8",
      "Accept" => "application/sparql-results+json",
    ],
  ]);

  $service_url = 'https://query.wikidata.org/sparql';

  $result = $api->post($service_url, $query);

  $matches = json_decode($result->response)->{'results'}->{'bindings'};

  //print "<pre>";
  //print_r($matches);

  output_resultlist($lang, $matches);
}

function output_header($lang, $bl_title, $selected_collections) {

  // Retrieve form data
  $searchfor = htmlspecialchars($_POST["q"]);
  $exclude_metadata_only = (array_key_exists('exclude_meta', $_POST)
      or ($_SERVER['REQUEST_METHOD'] == 'GET'))
    ? 'checked' : '';

  # override $selected_collections for POST requests
  if (isset($_POST['coll'])) {
    $selected_collections = $_POST['coll'];
  }

  $bl_description = ($lang == 'en')
    ? 'Search folders via linked Wikidata items.'
    : 'Suche Mappen über die verknüpften Wikidata-Items';
  $bl_hint = ($lang == 'en')
    ? '(At least 3 characters; use * for right-truncation)'
    : '(Mindestens drei Zeichen; benutze * für Rechtstrunkierung)';

echo <<<EOF
<!DOCTYPE html>
<html lang="$lang">
<head>
  <meta charset="UTF-8">
  <title>$bl_title</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
  <meta name="robots" content="noindex, nofollow"/>
  <link rel="stylesheet" href="styles/simple.css" />
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
</head>
<body>

<header id="title-block-header">
<h1 class="title">$bl_title</h1>
</header>
<p>$bl_description</p>

<form action="index.php" method="post">
<input type='hidden' id='lang' name='lang' value='$lang'>

<p style="margin-bottom:0.3em"><input type="text" size="50" id="q" name="q" value="$searchfor"></p>
<div class="hint">$bl_hint</div>
<p>
EOF;

  foreach ($GLOBALS['collections'] as $coll) {
    echo "<label class='nowrap' >" . $GLOBALS['collection'][$coll][$lang]
      . " <input type='checkbox' id='coll' name='coll[]' value='$coll'"
      . ((in_array($coll, $selected_collections)) ? ' checked' : '')
      . '></label> &#160; ';
  }

  $bl_action = ($lang == 'en')
    ? 'Search'
    : 'Suche';
  $bl_exclude_meta = ($lang == 'en')
    ? 'Exclude metadata-only records'
    : 'Reine Metadaten-Sätze ausschließen';

echo <<<EOF
</p>

<p><input type="checkbox" id="exclude_meta" name="exclude_meta" $exclude_metadata_only>
<label for="exclude_meta"> $bl_exclude_meta</label></p>

<button type="submit" style="margin-top:1em;">$bl_action</button>

</form>
EOF;
}

function output_resultlist($lang, $matches) {
  $bl_results = ($lang == 'en')
    ? 'Resultlist:'
    : 'Ergebnisse:';
  echo "<h3>$bl_results</h3>";
  echo '<ul>';
  $cnt = count($matches);
  foreach ($matches as $entry) {
    $url = $entry->pm20->value;
    $wd = $entry->item->value;
    $item_label = $entry->itemLabel->value;
    $collection = $entry->collection->value;

    $label = $item_label;
    if (isset($entry->pmLabel) and ($collection == 'pe' or $collection == 'co')) {
      $label = $entry->pmLabel->value;
    }
    $label = htmlentities($label, ENT_QUOTES, 'UTF-8', false);

    ## online document counts in WD are outdated!
    ##$online_count = (isset($entry->online))
    ##  ? '(' . $entry->online->value . ')&#160;'
    ##  : '';

    echo "<li><a href='$url'>$label</a>"
      , "&#160;&#160;<a href='$wd' title='Wikidata item: $item_label'><img src='images/Wikidata-logo.svg' class='inline-icon' alt='Wikidata Icon'></a></li>" ;
  }
  echo '</ul>';
  echo "$cnt ", ($lang == 'en') ? 'matches' : 'Treffer';
}

function output_footer($lang, $debug) {

  $bl_footer = ($lang == 'en')
    ? 'All folders of the <a href="https://en.wikipedia.org/wiki/20th_Century_Press_Archives">20th Century Press Archives</a> of <a href="https://www.zbw.eu/en">ZBW</a> (PM20) are linked to <a href="https://wikidata.org">Wikidata</a> items. The items include synonyms and other text in multiple languages, which is used for searching. More on PM20 in Wikidata <a href="https://www.wikidata.org/wiki/Wikidata:WikiProject_20th_Century_Press_Archives">here</a>.'
    : 'Alle Mappen des <a href="https://de.wikipedia.org/wiki/Pressearchiv_20._Jahrhundert">Pressearchiv 20. Jahrhundert</a> der <a href="https://www.zbw.eu/en">ZBW</a> (PM20) sind in <a href="https://wikidata.org">Wikidata</a>-Datenobjekten verlinkt. Diese Datenobjekte enthalten Synonyme und sonstigen Text in unterschiedlichen Sprachen, der zum Suchen benutzt wird. Mehr über Pressemappen in Wikidata <a href="https://www.wikidata.org/wiki/Wikidata:WikiProject_20th_Century_Press_Archives">hier</a>.';

  echo "<div class='hint'>&#160;</div>";
  echo "<div class='hint'>$bl_footer</div>";

  if ($debug) {
    echo <<<EOF

      <h3>Debug</h3>
      <pre>
    EOF;
    print_r( $_POST);
    echo '</pre>';
  }

  echo <<<EOF

    </body>
    </html>

  EOF;
}

